#!/bin/bash
#
# Aggmon Aggregated Discovery Low-Level Discovery Script for Zabbix
#
# Bartlomiej Kos, bkos@fast-stable-secure.net
#

###
### PRE-RUN
###

### DEBUG TOGGLE
# set -x

# EXTERNAL VARIABLES
xarg1="$1"
xarg2="$2"
xarg3="$3"

# INTERNAL VARIABLES
_snmpwalk_cmd1="snmpwalk -I h -O qUn -C c"
_snmpwalk_cmd2="snmpbulkwalk -I h -O qUn -C c"
_snmpwalk1=${_snmpwalk_cmd2}

declare -Ag _zbx_value2

###
### FUNCTIONS
###

extract_data1()
{
while read _snmpwalk_oid1 _snmpwalk_value1
do
	${_do_stage1}
	${_do_stage2}

	_zbx_value2[${_zbx_value1}]=${_snmpwalk_value3}

done << EOD
$(${_snmpwalk1} ${xarg1} ${xarg2} ${_oid1} 2> /dev/null | grep -Ev "(No Such|No More|End of MIB|Unknown Object Identifier)")
EOD
}

stage1_format1()
{
_zbx_value1="${_snmpwalk_oid1/*${_oid1}\./}"
_snmpwalk_value2="${_snmpwalk_value1//\"/}"
}

stage2_mtxrWlRtab1()
{
_snmpwalk_value3="${_snmpwalk_value2,,}"
_snmpwalk_value3="${_snmpwalk_value3// /:}"
_snmpwalk_value3="${_snmpwalk_value3%:}"
}

print_output1()
{
echo -e "{\n\t\"data\":["
_counter1=1
_counter2=${#_zbx_value2[*]}
for _index1 in ${!_zbx_value2[*]}
do
	echo -ne "\t{\t\"${_out_key1}\":\"${_index1}\",\t\"${_out_key2}\":\"${_zbx_value2[${_index1}]}\"\t}"
	if [ ${_counter1} -lt ${_counter2} ]; then echo -e ","; else echo -e ""; fi
	let _counter1++
done
echo -e "\t]\n}"
}

###
### LOOPS
###

case ${xarg3} in
	"cisco-eigrp")
		_oid1="1.3.6.1.4.1.9.9.449.1.4.1.1.3"
		_out_key1="{#SNMPINDEX}"
		_out_key2="{#CISCO_EIGRP_NEIGHBOR}"

		_format1="format_data1"
		_prepare1="prepare_cisco_eigrp1"
		
		extract_data1
		generate_output1
		;;
	"mtxrWlRtab1")
		_oid1="1.3.6.1.4.1.14988.1.1.1.2.1.1"
		_out_key1="{#SNMPINDEX}"
		_out_key2="{#MIKROTIK_AP_CLIENT}"

		_do_stage1="stage1_format1"
		_do_stage2="stage2_mtxrWlRtab1"

		extract_data1
		print_output1
		;;
	*)
	echo "STOOPID!"
	exit
	;;
esac

# EOF
