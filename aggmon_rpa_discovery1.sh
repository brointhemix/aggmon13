#!/bin/bash
#
# Aggmon13 Routing Protocol Adjacency Low-Level Discovery (Zabbix)
#
# Bartlomiej Kos, bkos@fast-stable-secure.net
#
# set -x

###
### PRE-RUN
###

snmp1="$1"
host1="$2"

###
### FUNCTIONS
###

###
### LOOPS
###

_variables1=(_routing_protocol_neighbour1)

for _index1 in ${_variables1[*]}; do declare -A ${_index1}; done

while read _oid1 _value1
do
	_oid_index1=$(echo ${_oid1} | cut -d '.' -f 16-)
	_oid_value1=( $(echo ${_value1} | sed -e 's/\"//g') )

	if [ -z ${_oid_value1[6]} ]
	then
		# IPv4
		_oid_value2=$(echo "$((16#${_oid_value1[0]})).$((16#${_oid_value1[1]})).$((16#${_oid_value1[2]})).$((16#${_oid_value1[3]}))")
	else
		# IPv6
		_counter1=0
		for _value1 in ${_oid_value1[*]};
		do
			_oid_value2="${_oid_value2}${_value1,,}"
			let _counter1++
			if [ ${_counter1} -eq 2 ]; then _oid_value2="${_oid_value2}:"; _counter1=0; fi
		done
		_oid_value2="${_oid_value2%:}"
	fi

	_routing_protocol_neighbour1[${_oid_index1}]=${_oid_value2}
	_oid_value2=""

done << EOD
$(snmpwalk -I h -O qUn -C c ${snmp1} ${host1} 1.3.6.1.4.1.9.9.449.1.4.1.1.3 | grep -Ev "(No Such|No More|End of MIB|Unknown Object Identifier)"; if [ $? -ne 0 ]; then exit 1; fi)
EOD

_routing_protocol_indetifier1="{#EIGRP_NEIGHBOUR}"

echo -e "{\n\t\"data\":["
_counter1=1
_counter_max_value1=${#_routing_protocol_neighbour1[*]}
for _index1 in ${!_routing_protocol_neighbour1[*]}
do
	# Watch out for the British spelling ;)
	echo -ne "\t{\t\"{#SNMPINDEX}\":\"${_index1}\",\t\"${_routing_protocol_indetifier1}\":\"${_routing_protocol_neighbour1[${_index1}]}\"\t}"
	if [ ${_counter1} -lt ${_counter_max_value1} ]; then echo -e ","; else echo -e ""; fi
	let _counter1++
done
echo -e "\t]\n}"

# EOF
