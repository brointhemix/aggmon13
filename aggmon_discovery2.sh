#!/bin/bash
#
# Aggmon Aggregated Discovery Low-Level Discovery Script for Zabbix
#
# Bartlomiej Kos, bkos@fast-stable-secure.net
#

###
### PRE-RUN
###

### DEBUG TOGGLE
set -x

# EXTERNAL VARIABLES
xvar1="$1"
xvar2="$2"
xvar3="$3"

# INTERNAL VARIABLES
_snmpwalk_cmd1="snmpwalk -I h -O qUn -C c"

declare -Ag _out_result1

###
### FUNCTIONS
###

extract_data1()
{
while read _snmpwalk_oid1 _snmpwalk_value1
do
	${_format1}
	${_prepare1}

	_out_result1[${_zbx_key_index1}]=${_zbx_key_value2}

	_zbx_key_value1=""
	_zbx_key_value2=""

done << EOD
$(${_snmpwalk_cmd1} ${xvar1} ${xvar2} ${_snmpwalk_oid1} | grep -Ev "(No Such|No More|End of MIB|Unknown Object Identifier)")
EOD
}

format_data1()
{
_zbx_key_index1="${_snmpwalk_oid1/*${_snmpwalk_oid1}\./}"
_zbx_key_value1=( ${_snmpwalk_value1//\"/} )
}

prepare_cisco_eigrp1()
{
if [ -z ${_zbx_key_value1[6]} ]
then
	# IPv4 peer address
	_zbx_key_value2=$(echo "$((16#${_zbx_key_value1[0]})).$((16#${_zbx_key_value1[1]})).$((16#${_zbx_key_value1[2]})).$((16#${_zbx_key_value1[3]}))")
else
	# IPv6 peer address
	_counter1=0
	for _value1 in ${_zbx_key_value1[*]};
	do
		_zbx_key_value2="${_zbx_key_value2}${_value1,,}"
		let _counter1++
		if [ ${_counter1} -eq 2 ]; then _zbx_key_value2="${_zbx_key_value2}:"; _counter1=0; fi
	done
	_zbx_key_value2="${_zbx_key_value2%:}"
fi
}

generate_output1()
{
echo -e "{\n\t\"data\":["
_counter1=1
_counter2=${#_out_result1[*]}
for _index1 in ${!_out_result1[*]}
do
        # Watch out for the British spelling ;)
        echo -ne "\t{\t\"${_out_key1}\":\"${_zbx_key_index1}\",\t\"${_out_key2}\":\"${_out_result1[${_index1}]}\"\t}"
        if [ ${_counter1} -lt ${_counter2} ]; then echo -e ","; else echo -e ""; fi
        let _counter1++
done
echo -e "\t]\n}"
}

###
### LOOPS
###

case ${xvar3} in
	"cisco-eigrp")
		_snmpwalk_oid1="1.3.6.1.4.1.9.9.449.1.4.1.1.3"
		_out_key1="{#SNMPINDEX}"
		_out_key2="{#CISCO_EIGRP_NEIGHBOR}"

		_format1="format_data1"
		_prepare1="prepare_cisco_eigrp1"
		
		extract_data1
		generate_output1
		;;
	"mikrotik-mtxrWlRtab")
		_snmpwalk_oid1="1.3.6.1.4.1.14988.1.1.1.2.1.1"
		_out_key1="{#SNMPINDEX}"
		_out_key2="{#MIKROTIK_WIRELES_CLIENT}"

		_format1="format_data1"

		extract_data1
		generate_output1
		;;
	*)
	echo "STOOPID!"
	exit
	;;
esac

# EOF
